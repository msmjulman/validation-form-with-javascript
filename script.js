const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const password = document.getElementById('password');
const passwordConf = document.getElementById('passwordConf');

// Validation en saisie
username.addEventListener('blur', (e) => {
	checkUsername(e);
});

email.addEventListener('blur', (e) => {
	checkEmail(e);
});

password.addEventListener('blur', (e) => {
	checkPassword(e);
});

passwordConf.addEventListener('blur', (e) => {
	checkPasswordConf(e);
});

// Validation a la soumission
form.addEventListener('submit', (e) => {
	checkUsername();
	checkEmail();
	checkPassword();
	checkPasswordConf();
	e.preventDefault();
});

// =================================================>
function checkUsername(event = null) {
	let usernameValue;
	if (!event === null) {
		usernameValue = event.target.value.trim();
	} else {
		usernameValue = username.value.trim();
	}
	// Username field
	if (usernameValue === '') {
		setErrorFor(username, 'Veuillez renseigner ce champs');
	} else {
		setSuccessFor(username);
	}
}

function checkEmail(event = null) {
	let emailValue;
	if (!event === null) {
		emailValue = e.target.value.trim();
	} else {
		emailValue = email.value.trim();
	}
	// Email field
	if (emailValue === '') {
		setErrorFor(email, 'Veuillez renseigner ce champs');
	} else if (!emailValid(emailValue)) {
		setErrorFor(email, 'Veuillez saisir un email valide');
	} else {
		setSuccessFor(email);
	}
}

function checkPassword(event = null) {
	let passwordValue;
	if (!event === null) {
		passwordValue = e.target.value.trim();
	} else {
		passwordValue = password.value.trim();
	}
	// Password field
	if (passwordValue === '') {
		setErrorFor(password, 'Veuillez renseigner ce champs');
	} else if (passwordValue.length < 6) {
		setErrorFor(
			password,
			'Le mot de passe doit contenir au moins de 6 caracteres'
		);
	} else if (!strongPasswordChecker(passwordValue)) {
		setErrorFor(
			password,
			'Le mot de passe doit contenir au moins une minuscule, une majuscule, un chiffre, une lettre, et un caracteres spéciaux (!, @, #, $, %, &)'
		);
	} else {
		setSuccessFor(password);
	}
}

function checkPasswordConf(event = null) {
	let passwordConfValue;
	if (!event === null) {
		passwordConfValue = e.target.value.trim();
	} else {
		passwordConfValue = passwordConf.value.trim();
	}
	// Password Confirmation field
	if (passwordConfValue === '') {
		setErrorFor(
			passwordConf,
			'Le champs mot de passe confirmation ne doit pas etre vide'
		);
	} else if (passwordConfValue !== password.value.trim()) {
		setErrorFor(passwordConf, 'Les deux mots de passe ne correspondent pas');
	} else {
		setSuccessFor(passwordConf);
	}
}

// =================================================>
function setErrorFor(input, message) {
	const formGroup = input.parentElement;
	formGroup.className = 'form-group error mb-0';

	const small = formGroup.querySelector('small');
	small.innerText = message;
}

// =================================================>
function setSuccessFor(input) {
	const formGroup = input.parentElement;
	formGroup.className = 'form-group success mb-0';
}

// =================================================>
function emailValid(address) {
	const regexEmail = /.+@.+\..+/;
	if (!regexEmail.test(address)) {
		return false;
	} else {
		return true;
	}
}

function strongPasswordChecker(pwd) {
	const regexPwd = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%&])/;
	if (!regexPwd.test(pwd)) {
		return false;
	} else {
		return true;
	}
}
